import React from 'react';
import {
  Bars,
  Nav,
  NavBtn,
  NavBtnLink,
  NavLink,
  NavMenu,
} from './NavbarElement';

const Navbar = () => {
  return (
    <>
      <Nav>
        <NavLink to="/">
          <h1>Logo</h1>
        </NavLink>
        <Bars />
        <NavMenu>
          <NavLink to="/about" activeStyles>
            About
          </NavLink>
          <NavLink to="/service" activeStyless>
            Service
          </NavLink>
          <NavLink to="/help" activeStyles>
            Help
          </NavLink>
          <NavLink to="/contact-us" activeStyles>
            Contect Us
          </NavLink>
          <NavLink to="/sign-up" activeStyles>
            Sign Up
          </NavLink>
        </NavMenu>
        <NavBtn>
          <NavBtnLink to="sign-in">Sign In</NavBtnLink>
        </NavBtn>
      </Nav>
    </>
  );
};

export default Navbar;
