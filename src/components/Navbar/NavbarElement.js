import styled from 'styled-components';
import { NavLink as Link } from 'react-router-dom';
import { FaBars } from 'react-icons/fa';

const Nav = styled.nav`
  background-color: BLACK;
  height: 80px;
  display: flex;
  justify-content: space-around;
  padding: 0.5rem calc((100vh - 1000px) / 2);
  z-index: 10;
`;
const NavLink = styled(Link)`
  color: WHITE;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  &:active {
    color: #15cdfc;
  }
`;

const Bars = styled(FaBars)`
  display: none;
  color: white;
  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

const NavMenu = styled.div`
  display: flex;
  align-items: center;
  margin-right: -24px;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 24px;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
const NavBtnLink = styled(Link)`
  border-radius: 4px;
  background-color: #256ce1;
  padding: 10px 22px;
  color: WHITE;
  border: none;
  outline: none;
  cursor: pointer;
  text-decoration: none;
  transition: all 0.2s ease-in-out;
  &:hover {
    transition: all 0.2s ease-in-out;
    background-color: WHITE;
    color: #010606;
  }
`;

export { Nav, NavLink, Bars, NavMenu, NavBtn, NavBtnLink };
